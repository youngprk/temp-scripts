This is a temporary script to keep track of time by alerting users for U of T Data Analytics Bootcamp

### Set Up ###
1. Save `TimeTracker.xlsx` as a `csv` file under the same level you are running the script
1. Run `python3 -m time_tracker`
