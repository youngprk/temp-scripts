import os
import subprocess
from datetime import datetime

import pandas as pd


def track_time(file_path):
    df = pd.read_csv(file_path)
    df = pd.DataFrame(df)
    df_time = df.iloc[2:, 0]
    df_message = df.iloc[2:, 2]

    for row, message in zip(df_time, df_message):
        while True:
            current_time = datetime.now().strftime("%Y-%m-%d %H:%M:00")
            schedule_time = pd.to_datetime(row)
            print(schedule_time, current_time)

            if current_time > str(schedule_time):
                break
            if current_time == str(schedule_time):
                subprocess.call(['./pop_message.sh', message])
                break
        print(row)


if __name__ == "__main__":
    time_tracker_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'TimeTracker.csv')
    track_time(time_tracker_file_path)



